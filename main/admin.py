from django.contrib import admin
from .models import Review, MalfunctionType, Malfunction, UserDetails, Questionare


class MalfunctionTypeAdmin(admin.ModelAdmin):
    list_display = ('title', 'equipment_type_rus')

class MalfunctionAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'price', 'eq_type')

    def eq_type(self, obj):
        return obj.type.equipment_type_rus()

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('addresser', 'isPublished', 'city', 'date')

class UserDetailsAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone_number', 'email', 'malfunction_type', 'equipment_type')

    def malfunction_type(self, obj):
        return obj.title

# Register your models here.
admin.site.register(Review, ReviewAdmin)
admin.site.register(MalfunctionType, MalfunctionTypeAdmin)
admin.site.register(Malfunction, MalfunctionAdmin)
admin.site.register(UserDetails, UserDetailsAdmin)
admin.site.register(Questionare)
