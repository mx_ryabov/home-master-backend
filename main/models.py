from django.db import models
from multiselectfield import MultiSelectField

EQUIPMENT_TYPES = (
    ('Dishwasher', 'Посудомоечная машина'),
    ('Hob', 'Варочная поверхность'),
    ('Oven', 'Духовой шкаф'),
    ('Refrigerator', 'Холодильник'),
    ('Washer', 'Стиральная машина'),
    ('Other', 'Другое')
)
EQUIPMENT_TYPES_ICONS = (
    ('switch-off', 'Не включается'),
    ('flow', 'Протекает'),
    ('noise', 'Сильно шумит'),
    ('without-water', 'Не набирает воду'),
    ('without-rotate', 'Не вращает барабан'),
    ('coffee-machine', 'Кофемашина'),
    ('tv', 'Телевизор'),
    ('other', 'Ледогенератор'),
    ('not-freeze', 'Не морозит'),
    ('freezes', 'Перемораживает'),
    ('doesnt-heat-water', 'Не греет воду'),
    ('without-water', 'Не отжимает'),
    ('knock-corks', 'Выбивает пробки'),
    ('switch-off', 'Отключается'),
    ('without-water', 'Не сливает воду'),
    ('water', 'Неприрывно заливает вода'),
    ('bad-wash', 'Плохо моет'),
    ('not-dry', 'Не сушит'),
    ('only-freezer', 'Работает только морозилка'),
    ('switch-off', 'Включается и сразу выключается'),
    ('ice-covered', 'Покрывается льдом'),
    ('not-freeze-upper-section', 'Не морозит верхняя камера'),
    ('switch-off', 'Не работает'),
    ('electric-shock', 'Бьёт током'),
    ('doesnt-work-burner', 'Не работает конфорка'),
    ('oven-doesnt-heat', 'Духовка не греет'),
    ('timer-doesnt-work', 'Таймер не работает'),
    ('oven-doesnt-close', 'Духовой шкаф не закрывается'),
    ('switcher-broken', 'Переключатель сломался'),
    ('burner-bad-heat', 'Конфорка плохо греет'),
    ('mm-doesnt-work', 'Модуль управления не работает'),
    ('sensor-doesnt-work', 'Не работает сенсор'),
    ('plate-gives-error', 'Плита выдает ошибку'),
    ('light-off', 'Не горит лампочка в духовке'),
    ('cracked-surface', 'Треснула поверхность'),
    ('electric-shock', 'Плита бьет током'),
    ('cracked-glass', 'Треснуло стекло духового шкафа')
)

# Create your models here.

class Review(models.Model):
    city = models.CharField(max_length=100, verbose_name=u"Город")
    text = models.TextField(blank=True, verbose_name=u"Текст отзыва")
    date = models.DateField(verbose_name=u"Дата")
    addresser = models.CharField(max_length=100, verbose_name=u"Имя отправителя", default=u"")
    isPublished = models.BooleanField(verbose_name=u"Опубликовано", default=True)

    def __str__(self):
        return '%s %s' % (self.addresser, self.city)

    class Meta:
        verbose_name = u"Отзыв"
        verbose_name_plural = u"Отзывы"


class MalfunctionType(models.Model):
    title = models.CharField(max_length=50, verbose_name=u"Название")
    icon_name = models.CharField(choices=EQUIPMENT_TYPES_ICONS, max_length=50, verbose_name=u"Иконка")
    equipment_type = MultiSelectField(choices=EQUIPMENT_TYPES, max_length=100, verbose_name=u"Тип техники")

    def __str__(self):
        return '%s | %s' % (self.title, self.equipment_type)

    def equipment_type_rus(self):
        result = ''
        for t in self.equipment_type:
            result += "%s | " % (str(list(filter(lambda et : t == et[0], EQUIPMENT_TYPES))[0][1]))
        return result

    

    class Meta:
        verbose_name = u"Тип неисправности"
        verbose_name_plural = u"Типы неисправностей"


class Malfunction(models.Model):
    title = models.CharField(max_length=150, verbose_name=u"Название")
    price = models.IntegerField(verbose_name=u"Цена", default=0)
    type = models.ForeignKey(MalfunctionType, verbose_name=u"Тип неисправности", on_delete=models.CASCADE)

    def __str__(self):
        return '%s за %s' % (self.title, self.price)

    class Meta:
        verbose_name = u"Неисправность"
        verbose_name_plural = u"Неисправности"


class UserDetails(models.Model):
    name = models.CharField(max_length=300, verbose_name=u"Имя")
    phone_number = models.CharField(max_length=50, verbose_name=u"Телефон")
    email = models.EmailField(verbose_name=u"Почта", blank=True, null=True)
    isQuestionare = models.BooleanField(default=False, verbose_name=u"Получено через опросник")
    malfunction_type = models.ForeignKey(
                                MalfunctionType, verbose_name=u"Тип неисправности", 
                                on_delete=models.CASCADE, blank=True, null=True
                            )
    equipment_type = models.CharField(
                                choices=EQUIPMENT_TYPES, max_length=100, 
                                verbose_name=u"Тип техники", blank=True, null=True
                            )
    

    def __str__(self):
        return '%s %s' % (self.name, self.phone_number)

    class Meta:
        verbose_name = u"Заявка на перезвон"
        verbose_name_plural = u"Заявки на перезвон"

class Questionare(models.Model):
    text = models.TextField(verbose_name=u"Текст комментария")
    date = models.DateField(verbose_name=u"Дата", auto_now=True)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = u"Комментарий в опроснике"
        verbose_name_plural = u"Комментарии в опроснике"